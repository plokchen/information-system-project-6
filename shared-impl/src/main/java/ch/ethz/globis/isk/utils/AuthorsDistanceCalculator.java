package ch.ethz.globis.isk.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import ch.ethz.globis.isk.domain.*;
import ch.ethz.globis.isk.persistence.PersonDao;

public class AuthorsDistanceCalculator {

	final HashMap<Person, Integer> visited;
	final PersonDao personDao;
	final Queue<Person> queue;
	
	public AuthorsDistanceCalculator(PersonDao personDao) {
		visited = new HashMap<Person, Integer>();
		queue = new LinkedList<Person>();
		this.personDao = personDao;
	}
	
	public Long getDistance(String startId, String endId){
		// Initialize
		Long result = (long) 0;
		Person startPerson = personDao.findOne(startId);
		Person endPerson = personDao.findOne(endId);
		
		queue.add(startPerson);
		visited.put(startPerson, 1);
		
		// Bread first search for the endPerson
		while (!queue.isEmpty()){
			Person currentPerson = queue.poll();
			
			if (currentPerson.equals(endPerson)){
				result = (long) visited.get(currentPerson);
				break;
			}
			
			for (Person nextPerson : getCoauthors(currentPerson)){
				if (!visited.keySet().contains(nextPerson)){
					queue.add(nextPerson);
					visited.put(nextPerson, visited.get(currentPerson) + 1);
				}
			}			
		}
		
		// reset both Datastructure
		visited.clear();
		queue.clear();
		
		// return the distance
		return result;
	}
	
	private Set<Person> getCoauthors(Person person){
		Set<Person> set = new HashSet<Person>();
    	
    	for (Publication publication : person.getAuthoredPublications()){
    		set.addAll(publication.getAuthors());
    	}
    	
    	set.remove(person);
    	
    	return set;
	}
}
