package ch.ethz.globis.isk.persistence.db4o;

import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.Publication;
import ch.ethz.globis.isk.domain.db4o.Db4oPerson;
import ch.ethz.globis.isk.persistence.PersonDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;
import ch.ethz.globis.isk.utils.AuthorsDistanceCalculator;

import org.springframework.stereotype.Repository;

import com.db4o.foundation.NotImplementedException;
import com.db4o.query.Query;

import java.util.*;

@Repository
public class Db4oPersonDao extends Db4oDao<String, Person> implements PersonDao {

    @Override
    public Person createEntity() {
        return new Db4oPerson();
    }

    @Override
    public Person findOneByName(final String name) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("name", new Filter(Operator.EQUAL, name));
        return findOneByFilter(filterMap);
    }

    @Override
    public Set<Person> getCoauthors(String id) {
    	// find the person with this id
    	Person person = findOne(id);
    	
    	// use the getCoauthors method and return the output
    	return getCoauthors(person);
    }

    @Override
    public Long computeAuthorDistance(String firstId, String secondId) {
    	AuthorsDistanceCalculator adc = new AuthorsDistanceCalculator(this);
    	return adc.getDistance(firstId, secondId);
    }

    @Override
    public Class getStoredClass() {
        return Db4oPerson.class;
    }

    private Set<Person> getCoauthors(Person person) {
//    	// create a new query and contrain it by the class Person
//    	Query query = db.query();
//        query.constrain(Db4oPerson.class);
//        
//        // build the query
//        query.descend("authoredPublications").descend("authors").constrain(person);
//        
//        // execute the query and convert it to an set
//        List<Person> list = query.execute();
//        Set<Person> set = new HashSet<Person>(list);
//        
//        // remove the person, which should not be included and return the set
//        set.remove(person);
//        return set;
    	
    	Set<Person> set = new HashSet<Person>();
    	
    	for (Publication publication : person.getAuthoredPublications()){
    		set.addAll(publication.getAuthors());
    	}
    	
    	set.remove(person);
    	
    	return set;
    }
}
