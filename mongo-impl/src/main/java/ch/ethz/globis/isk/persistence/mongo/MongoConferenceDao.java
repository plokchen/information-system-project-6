package ch.ethz.globis.isk.persistence.mongo;

import ch.ethz.globis.isk.domain.*;
import ch.ethz.globis.isk.domain.mongo.MongoConference;
import ch.ethz.globis.isk.domain.mongo.MongoProceedings;
import ch.ethz.globis.isk.persistence.ConferenceDao;
import ch.ethz.globis.isk.persistence.ProceedingsDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class MongoConferenceDao extends MongoDao<String, Conference> implements ConferenceDao {

    @Autowired
    ProceedingsDao proceedingsDao;

    @Override
    public Conference findOneByName(String name) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("name", new Filter(Operator.EQUAL, name));
        return findOneByFilter(filterMap);
    }

    @Override
    public Long countAuthorsForConference(String confId) {
    	return (long) findAuthorsForConference(confId).size();
    }

    @Override
    public Set<Person> findAuthorsForConference(String confId) {
//    	Set<Publication> publications = findPublicationsForConference(confId);
//    	HashSet<Person> results = new HashSet<Person>();
//    	
//    	for(Publication pu : publications){
//    		if(pu instanceof MongoProceedings){
//        		for(Person pe : pu.getEditors()){
//        			results.add(pe);
//        		}
//    		} else {
//        		for(Person pe : pu.getAuthors()){
//        			results.add(pe);
//        		}
//    		}
//    	}
//    	return results;
    	
    	HashSet<Person> set = new HashSet<Person>();
    	
    	Conference conference = findOne(confId);
    	
    	for(ConferenceEdition confEd : conference.getEditions()){
    		Proceedings proceedings = confEd.getProceedings();
    		
    		set.addAll(proceedings.getEditors());
    		
    		for(InProceedings inProceedings : proceedings.getPublications()){
    			set.addAll(inProceedings.getAuthors());
    		}
    	}
    	return set;
    }

    @Override
    public Set<Publication> findPublicationsForConference(String confId) {
//    	Set<ConferenceEdition> ce = findEditionsForConference(confId);
//    	String query = buildPublicationsForConferenceQuery(confId, ce);
//    	//System.out.println("findPublicationsForConference: " + query);
//    	HashSet<Publication> res = new HashSet<Publication>(mongoOperations.find(new BasicQuery(query), Publication.class, "publication"));
//    	//res.addAll(c)
//    	//System.out.println("findPublicationsForConference size: " + (res.size() + ce.size()) + "/" + ce.size());
//    	for(ConferenceEdition c : ce){
//    		String q = "{_id: \"" + c.getProceedings().getId() + "\"}";
//    		Publication p = mongoOperations.findOne(new BasicQuery(q), Publication.class, "publication");
//    		if(p != null) res.add(p);
//    	}
//    	
//    	return res;
    	Set<Publication> res = new HashSet<Publication>();
    	Set<ConferenceEdition> ces = findEditionsForConference(confId);
    	for(ConferenceEdition ce : ces){
    		res.add(ce.getProceedings());
    	}
    	return res;
    }

    @Override
    public Long countPublicationsForConference(String confId) {
    	Set<ConferenceEdition> ce = findEditionsForConference(confId);
    	String query = buildPublicationsForConferenceQuery(confId, ce);
    	return mongoOperations.count(new BasicQuery(query), "publication") + ce.size();
    }
    
    private Set<ConferenceEdition> findEditionsForConference(String confId) {
    	String query = "{\"conference.$id\": \"" + confId + "\"}"; 
    	return new HashSet<ConferenceEdition>(mongoOperations.find(new BasicQuery(query), ConferenceEdition.class, "conferenceEdition"));
    }
    
    private String buildPublicationsForConferenceQuery(String confId, Set<ConferenceEdition> ce){
    	String confEds = "";
    	
    	Iterator<ConferenceEdition> i = ce.iterator();
    	
    	while(i.hasNext()){
    		confEds = confEds + "\""+ i.next().getProceedings().getId() + "\"";
    		if(i.hasNext()) confEds = confEds + ", ";
    	}
    	
    	return "{\"parentPublication.$id\": { $in: [" + confEds +" ]}}";
    }

    @Override
    public Conference createEntity() {
        return new MongoConference();
    }

    @Override
    protected Class<MongoConference> getStoredClass() {
        return MongoConference.class;
    }

    @Override
    protected String collection() {
        return "conference";
    }
}
